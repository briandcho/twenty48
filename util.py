from collections import namedtuple as tpl
import os


class struct(dict):
  def __init_(self, *args, **kvps):
    super(struct, self).__init__(*args, **kvps)

  def __getattr__(self, key):
    return super(struct, self).get(key)

  def __setattr__(self, key, val):
    super(struct, self).__setitem__(key, val)


def chain(fns=[], params=[]):
  res = None
  for fn in fns:
    res = fn(*params)
    if res:
      return res
  return res


def tsil(*args):
  return list(reversed(*args))


def egnar(*args):
  return reversed(range(*args))


def p(x, y):
  return tpl('p', ['x', 'y'])(x, y)


def pget(b, p):
  return b[p.x][p.y]


def xyv(x, y, v):
  return struct(x=x, y=y, v=v)
