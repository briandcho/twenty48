import pytest
from unittest.mock import patch
from twenty48 import Board, R, D, L, U, WIDTH
from util import struct, xyv

X00, X10, X20, X30 = xyv(0, 0, 2), xyv(1, 0, 2), xyv(2, 0, 2), xyv(3, 0, 2)
X01, X02, X03, X13 = xyv(0, 1, 2), xyv(0, 2, 2), xyv(0, 3, 2), xyv(1, 3, 2)
X004, X104, X204 = xyv(0, 0, 4), xyv(1, 0, 4), xyv(2, 0, 4)
X014, X024, X034 = xyv(0, 1, 4), xyv(0, 2, 4), xyv(0, 3, 4)
X304, X308, X314 = xyv(3, 0, 4), xyv(3, 0, 8), xyv(3, 1, 4)


@pytest.mark.parametrize('tiles,direction,mvs', [
  (X00, L, []),
  (X00, R, [(X00, X30)]),
  (X00, U, []),
  (X00, D, [(X00, X03)]),
  (X30, L, [(X30, X00)]),
  (X30, R, []),
  (X03, U, [(X03, X00)]),
  (X03, D, []),
  ([X00, X304], R, [(X00, X20)]),
  ([X00, X304], L, [(X304, X104)]),
  ([X20, X304], L, [(X20, X00), (X304, X104)]),
  ([X00, X034], D, [(X00, X02)]),
  ([X00, X034], U, [(X034, X014)]),
  ([X00, X014], R, [(X00, X30), (X014, X314)]),
  ([X00, X014], D, [(X014, X034), (X00, X02)]),
  ([X30, X314], L, [(X30, X00), (X314, X014)]),
  ([X00, X10], D, [(X00, X03), (X10, X13)]),
  ([X03, X13], U, [(X03, X00), (X13, X10)]),
  ([X00, X10], R, [(X00, X104), (X104, X304)]),
  ([X00, X30], R, [(X00, X304)]),
  ([X304, X10, X20], R, [(X10, X204)]),
  ([X00, X10, X204, X304], R, [(X204, X308), (X00, X104), (X104, X204)]),
  ([X00, X10, X30], L, [(X10, X004), (X30, X10)]),
  #([X00, X10, X20, X30], R, [(X20, X304), (X00, X104), (X104, X204)]),
])
def test_swipe(tiles, direction, mvs):
  b = Board()
  add_tiles(b, tiles)
  assert b.swipe(direction) == mvs


@patch('twenty48.randint', side_effect=[0, 1, 0, 2, 1, 3])
def test_next_tile(xrandint):
  x, n = Board(), WIDTH * WIDTH
  assert next_tile_add(x) == X004
  xrandint.assert_any_call(1, 20)
  xrandint.assert_any_call(0, n - 1)
  assert next_tile_add(x) == X01
  xrandint.assert_any_call(0, n - 2)
  assert next_tile_add(x) == X03


def next_tile_add(b):
  t = struct(b.next_tile())
  b.add_tile(t)
  return t


def add_tiles(b, tiles):
  if not isinstance(tiles, list):
    tiles = [tiles]
  for t in tiles:
    b.add_tile(struct(t))

