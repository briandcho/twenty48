from itertools import chain, groupby
from random import randint
from util import p, pget, tsil, struct, xyv

WIDTH = 4
RANGE = list(range(WIDTH))
EGNAR = tsil(RANGE)
U, D, L, R = 'U', 'D', 'L', 'R'


class Board(object):
  def __init__(self):
    self.board = [[None for _ in RANGE]
                  for _ in RANGE]

  def next_tile(self):
    spaces_ = list(spaces(self.board))
    i = randint(0, len(spaces_) - 1)
    v = randint(1, 20)
    p = spaces_[i]
    return xyv(p.x, p.y, 4 if v == 1 else 2)

  def add_tile(self, tile=None):
    tile = tile or self.next_tile()
    self.board[tile.x][tile.y] = tile

  def swipe(self, dir_):
    def row_against(i):
      return [p(j, i) if is_lr else
              p(i, j) for j in range_against]

    is_lr, is_lu = dir_ in [L, R], dir_ in [L, U]
    range_against = RANGE if is_lu else EGNAR
    rows = [row_against(i) for i in RANGE]
    mvs = [self.coalesce(row) +
           self.compress(row) for row in rows]
    return list(chain(*mvs))

  def coalesce(self, row):
    tiles = [pget(self.board, p) for p in row
             if pget(self.board, p)]
    groups = [list(g) for _, g in
              groupby(tiles, lambda t: t.v)]
    groups = [g for g in groups if len(g) > 1]
    mvs = []
    for g in groups:
      src, dst = g[1], g[0]
      dst.v *= 2
      mvs.append(self.mv(src, dst))
    return mvs

  def compress(self, row):
    tiles = [pget(self.board, p) for p in row
             if pget(self.board, p)]
    mvs = []
    pairs = [(l, r) for l, r in zip(tiles, row)
             if (l.x, l.y) != (r.x, r.y)]
    for src, dstp in pairs:
      dst = struct(src, x=dstp.x, y=dstp.y)
      mvs.append(self.mv(src, dst))
    return mvs

  def mv(self, src, dst):
    b = self.board
    b[src.x][src.y], b[dst.x][dst.y] = None, dst
    return (src, dst)


def spaces(b):
  return (p(x, y) for x in RANGE for y in RANGE
          if not pget(b, p(x, y)))
