from itertools import product
from scene import Action, LabelNode, Scene, ShapeNode, run
from twenty48 import Board, R, D, L, U, WIDTH
from util import p
import ui

A = Action

CORNER_RADIUS = 7
ORIGIN = (0, 0)
GRAY = '#c9c9c9'
LIGHT_GRAY = '#e9e9e9'
DARK_GRAY = '#a9a9a9'
FONT = ('Arial Rounded MT Bold', 30)


class Twenty48(Scene):
  def setup(self):
    self.board_vw = self._fit_board_vw()
    self.tile_width = self.board_vw.size.w / (WIDTH + 1)
    self.board = Board()
    self._add_next_tile()
    self._add_next_tile()

  def touch_began(self, touch):
    self._last_loc = touch.location

  def touch_ended(self, touch):
    dir_, dist = dirdist(self._last_loc, touch.location)
    if dist > self.tile_width and self._mv_tiles(dir_):
      self._add_next_tile()
      self.did_change_size()

  def _fit_board_vw(self):
    w = min(self.size) * .89
    pos = (self.size.w/2, self.size.h*.4)
    board = square_of_squares(pos, w, WIDTH, GRAY, DARK_GRAY)
    self.add_child(board)
    return board

  def _mv_tiles(self, dir_):
    mvs = self.board.swipe(dir_)
    for mv in mvs:
      self._mv_tile(*mv)
    return mvs

  def _mv_tile(self, src, dst):
    pos = p2pos(dst, self.tile_width)
    actions = [A.move_to(*pos, .2)]
    if dst.vw != src.vw:
      actions.append(A.remove())
      self._add_next_tile(dst)
    src.vw.run_action(A.sequence(actions))

  def _add_next_tile(self, t=None):
    if not t:
      t = self.board.next_tile()
      self.board.add_tile(t)
    else:
      t.vw.run_action(A.remove())
    t.vw = tile_vw(t, self.tile_width)
    self.board_vw.add_child(t.vw)


def tile_vw(tile, w):
  label = LabelNode(str(tile.v), position=ORIGIN, color=DARK_GRAY, font=FONT)
  t = square_node(p2pos(tile, w), w, LIGHT_GRAY)
  t.add_child(label)
  t.alpha = 0
  t.run_action(A.sequence(A.wait(.1), A.fade_to(1.0, 0.35)))
  return t


def square_of_squares(position, width, n_rows, color, bg_color):
  sq = square_node(position, width, color)
  w = sq.size.w / (n_rows + 1)
  for i, j in product(range(n_rows), repeat=2):
    pos = p2pos(p(i, j), w)
    sq.add_child(square_node(pos, w, bg_color))
  return sq


def square_node(position, width, color):
  path = ui.Path.rounded_rect(0, 0, width, width, CORNER_RADIUS)
  return ShapeNode(path=path, position=position, fill_color=color)


def dirdist(frm, to):
  dx, dy = to.x - frm.x, to.y - frm.y
  if abs(dx) > abs(dy):
    return (R if dx > 0 else L), abs(dx)
  return (U if dy > 0 else D), abs(dy)


def p2pos(p, w):
  x0, y0, pad = -w*(WIDTH/2), w*(WIDTH/2), w/(WIDTH + 1)
  return (x0+pad+p.x*(w+pad), y0-pad-p.y*(w+pad))


if __name__ == '__main__':
  run(Twenty48(), show_fps=False)

